# README #

This is a jax-rs back end restful API seed code.

### Requirement ###
https://ffxblue.github.io/interview-tests/test/article-api/

### Features ###

* Validations (e.g. unique id, non-null field)
* Spring IOC
* Lombok
* Api/service/reposistory 3 layers design
* All in interface orientaed 
* Shipped with in-memory MongoDB for test and demo


### Assumptions ###

* unique id consisting of any string
* article title and data non-null
* date field includes time, which can be used in serach in future
* flexible json representation is required in future, so choose nosql as back end, however still plugable by implementsting new tag/article services.
* tag api is implementented as a realtimeview of the article table, no need sync
but at cost of slow read speed


### How to run ###

* Install JDK(>7)
* Install maven
* mvn jetty:run
* host up will be localhost:9000/articles

### TODO ###

* Swagger
* Spring security OAuth