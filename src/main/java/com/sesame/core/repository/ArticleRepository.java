package com.sesame.core.repository;

import com.sesame.core.entity.Article;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface ArticleRepository
  extends MongoRepository<Article, String>
{}
