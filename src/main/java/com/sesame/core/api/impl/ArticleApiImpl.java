package com.sesame.core.api.impl;

import com.sesame.core.api.ArticleApi;
import com.sesame.core.entity.Article;
import com.sesame.core.entity.ErrorMessage;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;
import com.sesame.core.service.ArticleService;
import com.sesame.core.util.Validator;
import java.util.Map;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;







public class ArticleApiImpl
  implements ArticleApi
{
  @Autowired
  ArticleService articleService;

  public Response getArticleById(String id)
  {
    try
    {
      return Response.status(200).type("application/json").entity(this.articleService.getArticleById(id)).build();
    }
    catch (NotFound e) {
      return Response.status(400).type("application/json").entity(new ErrorMessage(e.getMessage())).build();
    }
  }

  public Response postArticle(Article article) {
    Map errors = Validator.validate(article);
    if (errors != null) {
      return Response.status(400).type("application/json").entity(errors).build();
    }

    try
    {
      return Response.status(201).type("application/json").entity(this.articleService.insertArticle(article)).build();
    }
    catch (DuplicatedKey e) {
      return Response.status(400).type("application/json").entity(new ErrorMessage(e.getMessage())).build();
    }
  }
}
