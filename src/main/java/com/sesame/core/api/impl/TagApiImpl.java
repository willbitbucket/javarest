package com.sesame.core.api.impl;

import com.sesame.core.api.TagApi;
import com.sesame.core.entity.ErrorMessage;
import com.sesame.core.entity.Tag;
import com.sesame.core.service.TagService;
import com.sesame.core.service.impl.TagServiceImpl;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;






public class TagApiImpl
  implements TagApi
{
  public static Logger LOG = LoggerFactory.getLogger(TagServiceImpl.class);

  @Autowired
  TagService tagService;
  DateFormat formatter;

  public TagApiImpl()
  {
    this.formatter = new SimpleDateFormat("YYYY-MM-DD");
    this.formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  public Tag getTagByTagNameAndDate(String tagName, String date) {
    LOG.info("tag=" + tagName + ", date=" + date);
    Date dt = null;
    try {
      dt = this.formatter.parse(date);
    }
    catch (ParseException e) {
      Response.status(400).type("application/json").entity(new ErrorMessage(e.getMessage())).build();
    }
    return this.tagService.findTag(tagName, dt);
  }
}
