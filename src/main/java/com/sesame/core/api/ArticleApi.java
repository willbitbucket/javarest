package com.sesame.core.api;

import com.sesame.core.entity.Article;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Consumes({"application/json"})
@Produces({"application/json"})
public abstract interface ArticleApi
{
  @GET
  @Path("/articles/{id}")
  public abstract Response getArticleById(@PathParam("id") String paramString);
  
  @POST
  @Path("/articles")
  public abstract Response postArticle(Article paramArticle);
}
