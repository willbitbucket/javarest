package com.sesame.core.api;

import com.sesame.core.entity.Tag;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Consumes({"application/json"})
@Produces({"application/json"})
@Path("/tag")
public abstract interface TagApi
{
  @GET
  @Path("/{tagName}/{date}")
  public abstract Tag getTagByTagNameAndDate(@PathParam("tagName") String paramString1, @PathParam("date") String paramString2);
}
