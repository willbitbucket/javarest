package com.sesame.core.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;


public class Validator
{
  private static javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  public static Map validate(Object object) {
    if (validator == null) return null;
    Set errors = validator.validate(object, new Class[0]);
    if (errors.size() == 0) { return null;
    }
    Iterator<ConstraintViolation> iterator = errors.iterator();
    Map ret = new HashMap();
    while (iterator.hasNext()) {
      ConstraintViolation cv = (ConstraintViolation)iterator.next();
      ret.put(cv.getPropertyPath(), cv.getMessage());
    }
    return ret;
  }
}
