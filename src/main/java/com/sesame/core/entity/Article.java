package com.sesame.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Document(collection="articles")
public class Article {

    @NotNull
    private String id;
    @NotNull
    private String title;
    @NotNull
    @JsonFormat(shape=com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private java.util.Date date;
    private String body;
    private List<String> tags;

}
