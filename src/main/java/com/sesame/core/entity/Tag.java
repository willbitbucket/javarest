package com.sesame.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tag
{
private String tag;
private Integer count;
private List<String> articles;
private List<String> related_tags;

}
