package com.sesame.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.beans.ConstructorProperties;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class ErrorMessage
{
  private String error;

}
