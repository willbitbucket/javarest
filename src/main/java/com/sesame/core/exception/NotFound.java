package com.sesame.core.exception;


public class NotFound
  extends Exception
{
  public NotFound(Throwable cause)
  {
    super(cause);
  }

  public NotFound() {}

  public NotFound(String message)
  {
    super(message);
  }
}
