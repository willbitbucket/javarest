package com.sesame.core.exception;


public class DuplicatedKey
  extends Exception
{
  public DuplicatedKey(Throwable cause)
  {
    super(cause);
  }

  public DuplicatedKey() {}

  public DuplicatedKey(String message)
  {
    super(message);
  }
}
