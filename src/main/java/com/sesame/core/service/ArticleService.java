package com.sesame.core.service;

import com.sesame.core.entity.Article;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;

public abstract interface ArticleService
{
  public abstract Article getArticleById(String paramString)
    throws NotFound;
  
  public abstract Article insertArticle(Article paramArticle)
    throws DuplicatedKey;
}

