package com.sesame.core.service;

import com.sesame.core.entity.Tag;
import java.util.Date;

public abstract interface TagService
{
  public abstract Tag findTag(String paramString, Date paramDate);
}

