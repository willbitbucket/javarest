package com.sesame.core.service.impl;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.sesame.core.entity.Article;
import com.sesame.core.entity.Tag;
import com.sesame.core.entity.Tag.TagBuilder;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;
import com.sesame.core.service.TagService;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class TagServiceImpl implements TagService{

    public static Logger LOG = LoggerFactory.getLogger(TagServiceImpl.class);

    @Autowired
    MongoTemplate mongoTemplate;


    @Autowired
    Mongo mongo;


    @Value("${db}")
    String db;


    public Tag findTag(String tag, Date date)
    {
      DBCollection collection = this.mongo.getDB(this.db).getCollection("articles");
      LOG.info("Existing records");
      for (DBObject obj : collection.find()) {
        LOG.info(obj.toString());
      }

      BasicDBObjectBuilder match = new BasicDBObjectBuilder();
      match.push("$match");
      match.add("tags", tag);
      match.add("date", date);
      match.pop();

      BasicDBObject unwind = new BasicDBObject("$unwind", "$tags");

      BasicDBObjectBuilder group = new BasicDBObjectBuilder();

      group.push("$group");
      group.add("_id", tag);
      group.push("articles");
      group.add("$addToSet", "$_id");
      group.pop();
      group.push("related_tags");
      group.add("$addToSet", "$tags");
      group.pop();
      group.push("count");
      group.add("$sum", Integer.valueOf(1));

      group.pop();
      group.pop();

      List<DBObject> ops = new ArrayList();
      ops.add(match.get());
      ops.add(unwind);
      ops.add(group.get());

      LOG.info(tag + ", " + date + ": " + ops);
      System.out.println(tag + ", " + date + ": " + ops);

      Iterable<DBObject> iterable = collection.aggregate(ops).results();
      Iterator<DBObject> iterator = iterable.iterator();
      Tag tagRet = null;
      for (DBObject obj : iterable)
      {



        tagRet = Tag.builder().articles((List)obj.get("articles")).related_tags((List)obj.get("related_tags")).count((Integer)obj.get("count")).tag(tag).build();
      }


      return tagRet;
    }


    public Article getArticleById(String id)
        throws NotFound
      {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Article article = null;
        article = (Article)this.mongoTemplate.findOne(query, Article.class);
        if (article == null)
        {
          throw new NotFound("id " + id + " does not exist");
        }
        return article;
      }

      public Article insertArticle(Article article) throws DuplicatedKey {
        try {
          this.mongoTemplate.insert(article);
        } catch (DuplicateKeyException ex) {
          throw new DuplicatedKey(ex.getMessage());
        }
        return article;
      }
}
