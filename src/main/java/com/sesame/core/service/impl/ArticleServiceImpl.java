package com.sesame.core.service.impl;

import com.sesame.core.entity.Article;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;
import com.sesame.core.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;






public class ArticleServiceImpl
  implements ArticleService
{
  @Autowired
  MongoTemplate mongoTemplate;

  public Article getArticleById(String id)
    throws NotFound
  {
    Query query = new Query();

    query.addCriteria(Criteria.where("id").is(id));
    Article article = null;
    article = (Article)this.mongoTemplate.findOne(query, Article.class);
    if (article == null)
    {
      throw new NotFound("id " + id + " does not exist");
    }
    return article;
  }

  public Article insertArticle(Article article) throws DuplicatedKey {
    try {
      this.mongoTemplate.insert(article);
    } catch (DuplicateKeyException ex) {
      throw new DuplicatedKey(ex.getMessage());
    }
    return article;
  }
}
