package com.sesame.core.util;

import com.sesame.core.entity.Article;
import java.io.PrintStream;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;

public class ValidatorTest
{
    @Test
    public void testValidate()
            throws Exception
    {
        System.out.println(Validator.validate(new Article()));
        Assert.assertEquals(3, Validator.validate(new Article()).size());
    }
}
