package com.sesame.core;

/**
 * Created by Will on 2017/5/15.
 */


import org.junit.runner.RunWith;
import com.github.fakemongo.Fongo;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.io.PrintStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/spring/mongo-spring.xml"})
public class testMongoIT
{
    @Autowired
    Fongo fongo;
    @Autowired
    MongoTemplate mongoTemplate;

    public void basicOperations()
            throws Exception
    {
        DB db = this.fongo.getDB("mydb");
        DBCollection collection = db.getCollection("mycollection");
        collection.insert(new DBObject[] { new BasicDBObject("name", "jon") });

        DBCursor cursor = collection.find(new BasicDBObject("name", "jon"));
        if (cursor.hasNext())
        {
            DBObject obj = cursor.next();
            System.out.println(obj);
        }
    }
}
