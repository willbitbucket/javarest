package com.sesame.core.service.impl;

import com.sesame.core.entity.Article;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;
import com.sesame.core.service.ArticleService;
import java.io.PrintStream;
import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/spring/service-spring.xml", "classpath:/spring/mongo-spring.xml"})
public class ArticleServiceImplTest
{
    @Autowired
    ArticleService articleService;

    @Test
    public void testGetArticleById()
            throws Exception
    {
        Article article = new Article("122", "title", new Date(), "body", null);
        this.articleService.insertArticle(article);
        Article result = this.articleService.getArticleById("122");
        System.out.println(result);
        Assert.assertEquals("122", result.getId());
    }

    @Test(expected=NotFound.class)
    public void testGetArticleByIdFailed()
            throws Exception
    {
        Article result = this.articleService.getArticleById("1");
        System.out.println(result);
        Assert.assertEquals("1", result.getId());
    }

    @Test
    public void testPostArticle()
            throws Exception
    {
        Article article = new Article("111", "title", new Date(), "body", null);
        Article result = this.articleService.insertArticle(article);
        System.out.println(result);
        Assert.assertEquals(article, result);
    }

    @Test(expected=DuplicatedKey.class)
    public void testPostArticleDuplicated()
            throws Exception
    {
        Article article = new Article("123", "title", new Date(), "body", null);
        Article result = this.articleService.insertArticle(article);
        System.out.println(result);
        Article result2 = this.articleService.insertArticle(article);
        System.out.println(result2);
    }
}
