package com.sesame.core.service.impl;

import com.sesame.core.entity.Article;
import com.sesame.core.entity.Tag;
import com.sesame.core.exception.DuplicatedKey;
import com.sesame.core.exception.NotFound;
import com.sesame.core.service.ArticleService;
import com.sesame.core.service.TagService;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/spring/service-spring.xml", "classpath:/spring/mongo-spring.xml"})
public class TagServiceImplTest
{
    @Autowired
    TagService tagService;
    @Autowired
    ArticleService articleService;
    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void testGetTag()
            throws Exception
    {
        Date now = new Date();
        this.articleService.insertArticle(new Article("A22", "title", now, "body",
                new ArrayList() {{add("A");add("B");
                }}));
        this.articleService.insertArticle(new Article("B22", "title2", now, "body", new ArrayList() {
            {
                add("C");add("B");
            }
        }));
        this.articleService.insertArticle(new Article("C22", "title3", now, "body", new ArrayList() {
            {
                add("A");add("D");add("B");
            }
        }));
        System.out.println(this.mongoTemplate.findAll(Article.class));
        Tag tag = this.tagService.findTag("A", now);
        System.out.println(tag);
        Assert.assertEquals("A", tag.getTag());
        Assert.assertEquals(Integer.valueOf(5), tag.getCount());
        Assert.assertEquals(2L, tag.getArticles().size());

        tag = this.tagService.findTag("B", now);
        System.out.println(tag);
        Assert.assertEquals("B", tag.getTag());
        Assert.assertEquals(Integer.valueOf(7), tag.getCount());
        Assert.assertEquals(3L, tag.getArticles().size());
    }

    @Test(expected=NotFound.class)
    public void testGetArticleByIdFailed()
            throws Exception
    {
        Article result = this.articleService.getArticleById("1");
        System.out.println(result);
        Assert.assertEquals("1", result.getId());
    }

    @Test
    public void testInsertArticle()
            throws Exception
    {
        Article article = new Article("333", "title", new Date(), "body", null);
        Article result = this.articleService.insertArticle(article);
        System.out.println(result);
        Assert.assertEquals(article, result);
    }

    @Test(expected=DuplicatedKey.class)
    public void testPostArticleDuplicated()
            throws Exception
    {
        Article article = new Article("123", "title", new Date(), "body", null);
        Article result = this.articleService.insertArticle(article);
        System.out.println(result);
        Article result2 = this.articleService.insertArticle(article);
        System.out.println(result2);
    }
}
